
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
function greetings($nama){
    echo "Halo " . $nama . " Selamat Datang di Jabar Coding Camp!<br>";
}
greetings("Bagas");
greetings("Wahyu");
greetings("Abdul");

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";

function reverseString($nama){
    $reverse = "";
    for ($i = strlen($nama)-1;$i>=0; $i--){
        $reverse .=  $nama[$i];
    }
    echo "$reverse <br>";
}

reverseString("abduh");
reverseString("Bootcamp");
reverseString("We Are JCC Developers");

echo "<h3>Soal No 3 Palindrome </h3>";
echo "<br>";
function palindrome($nama){
    $namecheck = "";
    for ($i = strlen($nama)-1;$i>=0; $i--){
        $namecheck .=  $nama[$i];
    }
    echo ($nama == $namecheck) ? ("true") : ("false");
    echo "<br>";
}


palindrome("civic") ; // true
palindrome("nababan") ; // true
palindrome("jambaban"); // false
palindrome("racecar"); // true


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
function tentukan_nilai($nilai){
    if (($nilai>=85) && ($nilai<=100)){
        return "Sangat baik <br>";
    } else if (($nilai>=70) && ($nilai<85)){
        return "Baik <br>";
    } else if (($nilai>=60) && ($nilai<70)){
        return "Cukup <br>";
    } else return "Kurang <br>";

}

echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang


?>

</body>

</html>